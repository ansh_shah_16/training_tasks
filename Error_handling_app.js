const http = require('http')
const host = 'localhost'
const port = 8000

const server = http.createServer((req,res) => {
    try {
        res.statusCode = 200
        res.end('Hello World !')
    } catch (err) {
        res.statusCode = 404
        res.end(err)
    }
})
server.listen(port, host, () => {
    console.log(`server is up on http://${host}:${port}`)
})