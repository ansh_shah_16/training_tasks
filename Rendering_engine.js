const express = require("express")
const path = require("path")
const hbs = require('hbs');
const async = require("hbs/lib/async");
const app = express();
const port = process.env.PORT || 3000

const publicPath = path.join(__dirname,'./public')
app.use(express.static(publicPath))

// Rendering using 'express'
app.get('/', async (req,res) => {
    await res.send()
})

// rendering using 'hbs'
app.set('view engine','hbs')
app.set("views", path.join(__dirname, "views"))

app.get('/about',(req,res) => {
    try {
        res.render("about", {title: 'About Us'})
    } catch (err) {
        res.status(404).send("File Not Found!")
    }
})
app.get('/help',(req,res) => {
    try {
        res.render("help", {title: 'Help'})
    } catch (err) {
        res.status(404).send("File Not Found!")
    }
})
app.listen(port, () => {
    console.log("Server is up on : "+port)
});