const multer = require('multer')
const express = require('express')
const app = express()

const upload = multer({
    dest: 'profiles',
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, callback) {
        if (!file.originalname.match(/.(jpg|jpeg|png|JPG|JPEG|PNG)$/)) {
            return callback(new Error("Please upload Image!!"))
        }
        callback(undefined, true)
    }
})

app.post('/upload', upload.single('profile'), (req, res) => {
    res.send("Upload Successfully!")
}, (error, req, res, next) => {
    res.status(400).send({ Error: error.message })
})

app.listen(3000, () => {
    console.log("server is up on : 3000")
})